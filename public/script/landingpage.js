$(document).ready(function(){

  if (screen.width<992) {
    var a = window.height;
    var b = screen.height;
    $('#section0').css('height',a);
    $('#section1').css('height',b);
    $('#section2').css('height',b);
  }

  $(".what").click(function(){
    var scroll = $("#WhatIsEncirclo").offset();
    $('html,body').animate({
        scrollTop: scroll.top
    }, 800 );
  });

  $("#how").click(function(){
    var scroll = $("#HowItWorks").offset();
    $('html,body').animate({
        scrollTop: scroll.top
    }, 800 );
  });

  $(".join").click(function(){
    var scroll = $("#JoinUs").offset();
    $('html,body').animate({
        scrollTop: scroll.top
    }, 800 );
  });

});
