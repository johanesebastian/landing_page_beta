// $(".benefit-item").addClass("js");
var rotation = 0;
var winheight = $(window).height();
var scrolled;

$(window).scroll(function(){
  scrolled = $(window).scrollTop();
  console.log(scrolled + ' | ' + winheight/2);
  //detect apabila udah mencapai 50 persen dari layar
  var bool = $('#logo').hasClass('movecenter');
  // var options = 
  if(scrolled>(winheight/2)){
   if(!bool)
   {
    $('#encirclo').toggleClass('dissapear');
    $('#logo').toggleClass('movecenter');
   }
  } else {
    if(bool){
      $('#encirclo').toggleClass('dissapear');
      $('#logo').toggleClass('movecenter');
    }
  };
});

$.fn.animateRotate = function(angle, duration, easing, complete) {
  return this.each(function() {
    var $elem = $(this);
    $({deg: rotation}).animate({deg: angle}, {
      duration: duration,
      easing: easing,
      step: function(degrees) {
        $elem.css({
           transform: 'rotate(' + degrees + 'deg)'
         });
      },
      complete: complete || $.noop
    });
  });
};

$("#sign_up").click(function() {
    $('html,body').animate({
        scrollTop: $('#partnership').offset().top -70 }, 'slow');
    return false;
});

$(".about").click(function() {
    $('html,body').animate({
      scrollTop: $('#about_jump').offset().top -70 }, 'slow');
    return false;
});

$(".scrolltop").click(function() {
    $('html, body').animate({scrollTop : 0},800);
    return false;
});

jQuery.fn.rotate = function(degrees) {
    $(this).animate({
    	transform : 'rotate('+ degrees +'deg)'
    }, 400);
    return $(this);
};

function getRotationDegrees(obj) {
    var matrix = obj.css("-webkit-transform") ||
    obj.css("-moz-transform")    ||
    obj.css("-ms-transform")     ||
    obj.css("-o-transform")      ||
    obj.css("transform");
    if(matrix !== 'none') {
        var values = matrix.split('(')[1].split(')')[0].split(',');
        var a = values[0];
        var b = values[1];
        var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
    } else { var angle = 0; }
    return (angle < 0) ? angle + 360 : angle;
}

$(".dropbutton").click(function() {
	//Select Parentnya
	// var crntbutton =  getComputedStyle( $('.')[0], "")
  var init = (getRotationDegrees($(this)));
  console.log(init);
  var parentdiv = $(this).parent();

  //Select deskripsi tambahannya
  var next = parentdiv.next();
  init = init+180;

  $(this).animateRotate(init, 'medium');
  next.slideToggle('medium', function(){
    if ($(this).is(':visible'))
        $(this).css('display','flex');
  });
});

$('#burger_menu').click(function(){
  $('#dropmenu').slideToggle('medium');
})

$('#dropmenu > ul li a').click(function(){
  $('#dropmenu').slideToggle('medium');
})

$('#contact').click(function(){
  $('#dropmenu').slideToggle('medium');
})

$('#about').click(function(){
  $('#dropmenu').slideToggle('medium');
})

// $('#send').click(function(){
//   var count = 10;
//   var counter=setInterval(timer, 1000); //1000 will  run it every 1 second
//   function timer()
//   {
//     $('#roll_timer').animateRotate(360, 1000, 'linear');
//     count=count-1;
//     $('#timer').text(count);
//     if (count <= 0)
//     {
//        clearInterval(counter);
//        //counter ended, do something here
//        return;
//     }

//     //Do code for showing the number of seconds here
//   }
//   $('#thankyou').fadeIn(500);
//   timer();
//   $('#thankyou').delay(9000).fadeOut(500);
//   return false;
// })

// $('#ask').click(function(){
//   var count = 10;
//   var counter=setInterval(timer, 1000); //1000 will  run it every 1 second
//   function timer()
//   {
//     $('#roll_timer').animateRotate(360, 1000, 'linear');
//     count=count-1;
//     $('#timer').text(count);
//     if (count <= 0)
//     {
//        clearInterval(counter);
//        //counter ended, do something here
//        return;
//     }

//     //Do code for showing the number of seconds here
//   }
//   $('#thankyou').fadeIn(500);
//   $('#thank_image').delay(1000).toggleClass('open');
//   timer();
//   $('#thankyou').delay(9000).fadeOut(500, function(){
//     $('#thank_image').toggleClass('open');
//   });
//   return false;
// })

$(document).ready(function(){
})
