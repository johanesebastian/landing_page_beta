$(function() {

    $("input,textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM

            var name = $("input#firstname").val();
            var email = $("input#email").val();
            var phone = $("input#phone_number").val();
            var message = $("textarea#lastname").val();
            // var firstName = name; // For Success/Failure Message
            // Check for white space in name for Success/Fail message
            // if (firstName.indexOf(' ') >= 0) {
            //     firstName = name.split(' ').slice(0, -1).join(' ');
            // }
            $.ajax({
                url: "././mail/contact_me2.php",
                type: "POST",
                data: {
                    name: name,
                    phone: phone,
                    email: email,
                    message: message
                },
                cache: false,
                success: function() {
                    // Success message
                      var count = 10;
                      var counter=setInterval(timer, 1000); //1000 will  run it every 1 second
                      function timer()
                      {
                        $('#roll_timer').animateRotate(360, 1000, 'linear');
                        count=count-1;
                        $('#timer').text(count);
                        if (count <= 0)
                        {
                           clearInterval(counter);
                           //counter ended, do something here
                           return;
                        }

                        //Do code for showing the number of seconds here
                      }
                      $('#thankyou').fadeIn(500);
                      $('#thank_image').delay(1000).toggleClass('open');
                      timer();
                      $('#thankyou').delay(9000).fadeOut(500, function(){
                        $('#thank_image').toggleClass('open');
                      });
                      // return false;

                    //clear all fields
                    $('#askForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Sorry " + firstName + ", it seems that my mail server is not responding. Please try again later!");
                    $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#askForm').trigger("reset");
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});
