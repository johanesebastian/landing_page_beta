$(function() {

    $("input,textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM

            var firstname = $("input#firstname").val();
            var email = $("input#email").val();
            var phone_number = $("input#phone_number").val();
            var jabatan = $("select#jabatan").val();
            var jabatanlain = $("input#others").val();
            var nama_perusahaan = $("input#nama_perusahaan").val();
            var lastname = $("textarea#lastname").val();
            var website = $("input#website").val();
            // var firstName = name; // For Success/Failure Message
            // Check for white space in name for Success/Fail message
            // if (firstName.indexOf(' ') >= 0) {
            //     firstName = name.split(' ').slice(0, -1).join(' ');
            // }
            $.ajax({
                url: "././mail/contact_me.php",
                type: "POST",
                data: {
                    firstname: firstname,
                    email: email,
                    phone_number: phone_number,
                    jabatan: jabatan,
                    jabatanlain: jabatanlain,
                    nama_perusahaan: nama_perusahaan,
                    lastname: lastname,
                    website: website
                },
                cache: false,
                success: function() {
                    // Success message
                    var count = 10;
                    var counter=setInterval(timer, 1000); //1000 will  run it every 1 second
                    function timer()
                    {
                        $('#roll_timer').animateRotate(360, 1000, 'linear');
                        count=count-1;
                        $('#timer').text(count);
                        if (count <= 0)
                        {
                            clearInterval(counter);
                            //counter ended, do something here
                            return;
                        }

                        //Do code for showing the number of seconds here
                    }
                    $('#thankyou').fadeIn(500);
                    timer();
                    $('#thankyou').delay(9000).fadeOut(500);
                    // return false;
                    
                    // window.location.href = "subscribed.html";

                    //clear all fields
                    document.getElementById("contactForm").reset();
                    // $('#contactForm').trigger("reset");
                },
                error: function() {
                    // Fail message
                    window.location.href = "error.html";
                    // $('#success').html("<div class='alert alert-danger'>");
                    // $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                    //     .append("</button>");
                    // $('#success > .alert-danger').append("<strong>Sorry " + firstName + ", it seems that my mail server is not responding. Please try again later!");
                    // $('#success > .alert-danger').append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});
