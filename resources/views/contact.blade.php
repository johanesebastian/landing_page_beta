<html>
  <head>
    <meta charset="utf-8">
    <title>Contact Us</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- BOOTSTRAP CDN -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- link jquery -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <!-- STYLE -->
    <link rel="stylesheet" href="{!! url('css/landingpage.css') !!}">
    <link rel="stylesheet" href="{!! url('css/medquery-lp.css') !!}">
    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,900,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Anonymous+Pro|Raleway:400,600,700' rel='stylesheet' type='text/css'>
     <!-- Latest compiled and minified JavaScript -->
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script>
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

     ga('create', 'UA-72505768-1', 'auto');
     ga('send', 'pageview');

   </script>
    <script>
    function terimakasih() {
      $.ajax({
        url:'{!! url('contact') !!}',
        type:"post",
        data:$('#formulir').serialize()
      });
      document.getElementById("terimakasih1").style.display = "block";
      $('#formulir .required').val('');
      $('#formulir textarea').val('');
      return false;
    }
    </script>
      <!-- Histats.com  START  (aync)-->
    <script type="text/javascript">var _Hasync= _Hasync|| [];
    _Hasync.push(['Histats.start', '1,3458701,4,0,0,0,00010000']);
    _Hasync.push(['Histats.fasi', '1']);
    _Hasync.push(['Histats.track_hits', '']);
    (function() {
    var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
    hs.src = ('//s10.histats.com/js15_as.js');
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
    })();</script>
    <noscript><a href="http://www.histats.com" target="_blank"><img  src="http://sstatic1.histats.com/0.gif?3458701&101" alt="unique visitors counter" border="0"></a></noscript>
    <!-- Histats.com  END  -->
  </head>
  <body style="height:100%">
  <header>
    <div id="header">
      <nav class="navbar navbar-default navbar-fixed-top">
    			<div class="container-fluid">
    	 <!-- Brand and toggle get grouped for better mobile display -->
    			<div class="navbar-header">
    						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#burger-collapse">
    							<span class="icon-bar"></span>
    							<span class="icon-bar"></span>
    							<span class="icon-bar"></span>
    						</button>
    						<a class="navbar-brand" href='{!!url ('/')!!}'>
    							<img src="img/logofull_inversetransp-01.png">
    						</a>
    			</div>
    			<!-- Collect the nav links, forms, and other content for toggling -->
    			<div class="navbar-collapse collapse" id="burger-collapse">
    				<ul class="nav navbar-nav navbar-right">
    					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">About Us <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{!!url ('/#WhatIsEncirclo')!!}">What is Encirclo</a></li>
                  <li><a href="{!!url ('/#HowItWorks')!!}">How It Works</a></li>
                  <li><a href="{!!url ('vip')!!}">VIP</a></li>
    							<li><a href='ourvalues'>Our Values</a></li>
                </ul>
    						<li><a href="{!!url ('/#JoinUs')!!}" id='contact'>Join Us</a></li>
    						<li><a href='contact' id='contact'>Contact Us</a></li>
    				</ul>
    			</div><!-- /.navbar-collapse
    			</div><!-- /.container -->
    		</nav>
      </div>
    </header>

    <div class="kontenhubungikami">
      <div class="judulkontak">
        <a href="{!!url ('/')!!}" class="btn back">Back</a>
        <h1>Contact Us</h1>
        <p>Feel free to ask and let us know what you think about Encirclo.</p>
        <p>Your feedback will be very helpful for our website improvement. With respect, thank you for not spamming.</p>
      </div>

      <div class="row">
        <div class="col-sm-6 left">
          <form id="formulir" onsubmit="return terimakasih()">
            <input type="text" name="nama" placeholder="Name*" class="required form-control">
            <input type="email" name="email" placeholder="Email*" class="required email form-control">
            <input type="text" name="telepon" placeholder="Phone Number" class="required form-control">
            <textarea type="text" name="message" placeholder="Message" class="form-control"></textarea>
            <input class='btn button submit' id='ask' type='submit' name='kirim' value='SUBMIT' data-toggle="modal" data-target="#myModal">
          </form>
          <p id="terimakasih1" style="display:none;font-size:11px;margin-bottom:25px;"> Thank you for contacting us. We will response your message as soon as possible.</p>
        </div>
        <div class="col-sm-6 right">
          <div class='company-info'>
  				<ul>
            <p style="font-size:15px;"> <b>Information:</b> </p>
  					<li class="info">
  						<img src='img/buttonlocation.png'>
  					</li>
            <span><b>Address:</b> Graha Komando<br>Jalan Raya Cipinang Indah<br> Jakarta Timur</span>
  					<li class="info">
  						<img src='img/buttontele.png'>
  					</li>
            <span><b>Tel:</b> (021) 2936 1198/99<br><b>Fax:</b> (021) 2936 1194</span>
  					<li class="info">
  						<img src='img/buttonemail.png'>
  					</li>
            <span><b>Inquiry:</b> info@encirclo.com<br><b>Partnership:</b> network@encirclo.com</span>
  				</ul>
  			</div>
      </div>
    </div>


      </div>
  <footer>
		<div class="row ">
			<div class="col-xs-6 kolom1" style="margin-top:15px">
				<h1 class="footerh1"> E N C I R C L O </h1>
				<h2>An Indonesian small company with huge dreams to change the way people do business all over the world. We won't stop until all pieces unite and collaborate in one place for goodness.</h2>
			</div>
			<!--------------------DIV DIATAS ADALAH Footer kolom kiri -------------------->
			<div class="col-xs-6 kolom2">
				<div class="rowatas">
					<h5 style="color:white; letter-spacing:2px;">Discover More</h5>
					<ul class="">
						<!-- <li class='footerbaris'>Discover More</li> -->
						<li class='gambar'>
							<a id='fb' href='https://www.facebook.com/encirclo' target='_blank'>
								<img src="img/buttonfb6.png" alt='facebook'>
							</a>
						</li>
						<li class='gambar'>
							<a id='tw' href='https://www.twitter.com/encirclomedia' target='_blank'>
								<img src="img/buttontw6.png" alt="twitter">
							</a>
						</li>
						<li class='gambar'>
							<a id='in' href='https://www.linkedin.com/company/pt--encirclo-nusa-integra?trk=top_nav_home' target='_blank'>
								<img src="img/buttonin6.png" alt="linkedin">
							</a>
						</li>
					</ul>
				</div>
				<div class="rowbawah">
					<ul>
						<li><a href="{!!url('terms-condition') !!}">Terms and Conditions</a></li>
						<li><a href="{!!url('privacy') !!}">Privacy Policy</a></li>
						<li><a href="{!!url('help') !!}">Help Center</a></li>
					</ul>
					<span> &copy; PT. Encirclo Nusa Integra. All Rights Reserved</span>
				</div>
			</div>
		</div>
	<!-----------------------DIV DIATAS ADALAH Footer kolom kanan bawah---------------------->
	</footer>
</body>
</html>
