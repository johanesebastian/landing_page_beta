<html>
<head>
	<!-- V.1 -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Encirclo is a business networking platform for Startup,  Small Business, Medium Enterprise, even Big Company.">
	<meta name="author" content="Encirclo">

	<title>Encirclo</title>
	<link rel="icon" href="{!! url('img/logo.png') !!}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	<!-- GOOGLE FONT -->
	<link href='https://fonts.googleapis.com/css?family=Roboto:300,900,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Anonymous+Pro|Raleway:400,600,700' rel='stylesheet' type='text/css'>
	<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="{!! url('css/landingpage.css') !!}">
	<link rel="stylesheet" type="text/css" href="{!! url('css/medquery-lp.css') !!}">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

		<!-- Histats.com  START  (aync)-->
	<script type="text/javascript">var _Hasync= _Hasync|| [];
	_Hasync.push(['Histats.start', '1,3458701,4,0,0,0,00010000']);
	_Hasync.push(['Histats.fasi', '1']);
	_Hasync.push(['Histats.track_hits', '']);
	(function() {
	var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
	hs.src = ('//s10.histats.com/js15_as.js');
	(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
	})();</script>
	<noscript><a href="http://www.histats.com" target="_blank"><img  src="http://sstatic1.histats.com/0.gif?3458701&101" alt="unique visitors counter" border="0"></a></noscript>
	<!-- Histats.com  END  -->
	<script>
		 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		 ga('create', 'UA-72505768-1', 'auto');
		 ga('send', 'pageview');
	 </script>
</head>

<body>
	<div id="header">
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#burger-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href=''>
						<img src="img/logofull_inversetransp-01.png">
					</a>
				</div>
				<div class="navbar-collapse collapse" id="burger-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">About Us
								<span class="caret">
								</span>
							</a>
	            <ul class="dropdown-menu sub-menu-nav">
								<li><a class="what">What is Encirclo</a></li>
	              {{-- <li><a id="what" href="#WhatIsEncirclo">What is Encirclo</a></li> --}}
	              <li><a id="how">How It Works</a></li>
								{{-- <li><a id="how" href="#HowItWorks">How It Works</a></li> --}}
								<li><a href="{!!url ('vip')!!}">VIP</a></li>
								<li><a href='ourvalues'>Our Values</a></li>
	            </ul>
						<li><a class='join'>Join Us</a></li>
						{{-- <li><a href='#JoinUs' id='join'>Join Us</a></li> --}}
						<li><a href='contact' id='contact'>Contact Us</a></li>
					</ul>
				</div><!-- navbar-collapse -->
			</div><!-- div container-fluid -->
		</nav>
	</div><!-- div header -->

	<div class="section" id="Home">
		<div class="intro jumbotron">
			<h1>Welcome To The New Age of </h1>
			<h1 class="sub_h1">Business Networking</h1>
			<p>Build trust and professional relationship of your business.</p>
			<p>Simply by creating profile, access any information, and collaborate with relevant partners.</p>
		</div><!-- div jumbotron -->
		<div class="bottom-section">
			<a class="join btn1"><b>Be Our First 50 VIP</b></a>
			<a class="btn glyphicon glyphicon-chevron-down what" aria-hidden="true"></a>
		</div>
	</div><!-- div Home -->
	<div class="section" id="WhatIsEncirclo">
		<div class="intro">
			<img src="img/heroshot.png" id="heroshot" />
			<h1 class="sub_h1what">Encirclo is a business networking platform</h1>
			<ul>
				<li>Startup</li>
				<li>Small Business</li>
				<li>Medium Enterprise</li>
				<li>Big Company</li>
			</ul>
			<p class="sub_pwhat">We are here to simplify business activities and networking for them. Getting information, interaction, partner, customer, client, business opportunity, project, event, sales, marketing, supply, demand, literally anything for business. Not just a platform, but we are beneficial business partner. </p>
		</div>
	</div>
	<div class="section" id="HowItWorks">
		<div class="container sliding">
			<div id="myCarousel" data-interval="false" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
		      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		      <li data-target="#myCarousel" data-slide-to="1"></li>
		      <li data-target="#myCarousel" data-slide-to="2"></li>
				</ol>
				<!-- Wrapper for slides -->
			  <div class="carousel-inner" style="">
			    <div class="item active">
						<div class="slide" id="slide1">
							<h1 class="sub_h1how">Create Your Business Profile</h1>
							<div class="row">
								<div class="col-sm-4">
									<img src="img/mengapa3-01.png">
									<h3>Professional Profile</h3><br>
									<p>The more detailed and reliable, the greater chance of getting business network</p>
								</div>
								<div class="col-sm-4">
									<img src="img/mengapa3-02.png">
									<h3>Active Sharing</h3><br>
									<p>Share any beneficial information about business</p>
								</div>
								<div class="col-sm-4">
									<img src="img/mengapa3-03.png">
									<h3>Security</h3><br>
									<p>We always honor and protect confidential information of your business</p>
								</div>
							</div><!-- div konten1 -->
						</div><!-- div slide1 -->
					</div><!-- div item active -->
    			<div class="item">
						<div class="slide" id="slide2">
							<h1 class="sub_h1how">Access Business Information</h1>
							<div class="row">
								<div class="col-sm-4">
									<img src="img/mengapa3-04.png">
									<h3>Visit</h3><br>
									<p>You will have direct access to any business profile, projects, products, and services</p>
								</div>
								<div class="col-sm-4">
									<img src="img/mengapa3-05.png">
									<h3>Gather Information</h3><br>
									<p>Follow-up any informations that will support your business sustainability</p>
								</div>
								<div class="col-sm-4">
									<img src="img/mengapa3-06.png">
									<h3>Find Partner</h3><br>
									<p>Find opportunities and build network that relevant to your business</p>
								</div>
							</div>
						</div><!-- div slide2 -->
    			</div><!-- div item -->
			    <div class="item">
						<div class="slide" id="slide3">
							<h1 class="sub_h1how">Collaborate With Partner</h1>
							<div class="row">
								<div class="col-sm-4">
									<img src="img/mengapa3-07.png">
									<h3>Build Trust</h3><br>
									<p>Be truthful and consistent, they will trust a credible and reliable business</p>
								</div>
								<div class="col-sm-4">
									<img src="img/mengapa3-08.png">
									<h3>Communicate</h3><br>
									<p>Send professional message to let them know your interest</p>
								</div>
								<div class="col-sm-4">
									<img src="img/mengapa3-09.png">
									<h3>Make a deal</h3><br>
									<p>Obtain a business deal in Encirclo before you do the “handshake”</p>
								</div>
							</div>
						</div><!-- div slide3 -->
    			</div><!-- div item -->
	    		<!-- Left and right controls -->
					<div class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			      <span class="sr-only">Previous</span>
			    </div>
			    <div class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			      <span class="sr-only">Next</span>
			    </div>
				</div><!-- div carousel-inner -->
			</div><!-- div mycarousel -->
		</div><!-- div container-sliding -->
	</div><!-- div HowItWorks -->
	<div class="section" id="JoinUs">
		<div class="intro">
			<!-- Begin MailChimp Signup Form -->
			<div id="mc_embed_signup">
				<form action="//encirclo.us13.list-manage.com/subscribe/post?u=51468d8beeca479e9b88e99fc&amp;id=f1e53c15af" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<div id="mc_embed_signup_scroll">
					<h2>Be Our First <a href="{!!url('vip') !!}">VIP</a></h2>
					<p>Subscribe your business to get promoted FOR FREE and receive our website updates!</p>
					<div class="indicates-required"><span class="asterisk">*</span> By clicking subscribe you are agree to the <a href="{!!url('terms-condition') !!}">Terms & Conditions</a> and <a href="{!!url('privacy') !!}">Privacy Policy </a>
					</div>
					<div class="mc-field-group">
						<input type="text" value="" name="FNAME" class="required" id="mce-FNAME" placeholder="Full Name*">
					</div>
					<div class="mc-field-group"> <!-- size1of2 -->
						<input type="number" name="PHONE" class="required" value="" id="mce-PHONE" placeholder="Phone Number*">
					</div>
					<div class="mc-field-group">
						<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address*">
					</div>
					<div class="mc-field-group">
						<input type="text" value="" name="MMERGE2" class="required" id="mce-MMERGE2" placeholder="Company Name*">
					</div>
					<div class="mc-field-group">
						<input type="text" value="" name="DESC" class="" id="mce-DESC" placeholder="Company Short Description">
					</div>
					<div class="mc-field-group">
						<select name="POSITION" class="required" id="mce-POSITION">
							<option value="">Position*</option>
							<option value="C-Level (Chief..)">C-Level (Chief..)</option>
							<option value="Director / Vice President">Director / Vice President</option>
							<option value="Manager">Manager</option>
							<option value="Staff">Staff</option>
							<option value="Other">Other</option>
						</select>
					</div>
					<div id="mce-responses" class="clear">
						<div class="response" id="mce-error-response" style="display:none"></div>
						<div class="response" id="mce-success-response" style="display:none"></div>
					</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
				    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_51468d8beeca479e9b88e99fc_f1e53c15af" tabindex="-1" value=""></div>
				    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
					</div><!-- div mc_embed_signup_scroll -->
				</form>
			</div><!-- div mc_embed_signup -->
				<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
				<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[1]='FNAME';ftypes[1]='text';fnames[3]='PHONE';ftypes[3]='number';fnames[0]='EMAIL';ftypes[0]='email';
				fnames[2]='MMERGE2';ftypes[2]='text';fnames[4]='DESC';ftypes[4]='text';fnames[5]='POSITION';ftypes[5]='dropdown';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
				<!--End mc_embed_signup-->
		</div><!-- div intro -->
	</div><!-- div JoinUs -->

	<!---------------------- THIS IS FOOTER  ------------------------------------>
	<footer>
		<div class="row ">
			<div class="col-sm-6 kolom1" style="margin-top:15px">
				<h1 class="footerh1"> E N C I R C L O </h1>
				<h2>An Indonesian small company with huge dreams to change the way people do business all over the world. We won't stop until all pieces unite and collaborate in one place for goodness.</h2>
			</div>
			<!--------------------DIV DIATAS ADALAH Footer kolom kiri -------------------->
			<div class="col-sm-6 kolom2">
				<div class="rowatas">
					<h5 style="color:white; letter-spacing:2px;"> Discover More</h5>
					<ul class="">
						<!-- <li class='footerbaris'>Discover More</li> -->
						<li class='gambar'>
							<a id='fb' href='https://www.facebook.com/encirclo' target='_blank'>
								<img src="img/buttonfb6.png" alt='facebook'>
							</a>
						</li>
						<li class='gambar'>
							<a id='tw' href='https://www.twitter.com/encirclomedia' target='_blank'>
								<img src="img/buttontw6.png" alt="twitter">
							</a>
						</li>
						<li class='gambar'>
							<a id='in' href='https://www.linkedin.com/company/pt--encirclo-nusa-integra?trk=top_nav_Home' target='_blank'>
								<img src="img/buttonin6.png" alt="linkedin">
							</a>
						</li>
					</ul>
				</div>
				<div class="rowbawah">
					<ul>
						<li><a href="{!!url('terms-condition') !!}">Terms and Conditions</a></li>
						<li><a href="{!!url('privacy') !!}">Privacy Policy</a></li>
						<li><a href="{!!url('help') !!}">Help Center</a></li>
					</ul>
					<span> &copy; PT. Encirclo Nusa Integra. All Rights Reserved</span>
				</div>
			</div>
		</div>
	<!-----------------------DIV DIATAS ADALAH Footer kolom kanan bawah---------------------->
	</footer>

</body>

<!-- SCRIPT -->
<script type="text/javascript" src="{!! url('script/landingpage.js') !!}" ></script>
<!-- Latest compiled and minified JavaScript -->
</html>
