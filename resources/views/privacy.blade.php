<html>
  <head>
    <meta charset="utf-8">
    <title>Privacy Policy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- BOOTSTRAP CDN -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- link jquery -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <!-- STYLE -->
    <link rel="stylesheet" href="{!! url('css/landingpage.css') !!}">
    <link rel="stylesheet" href="{!! url('css/medquery-lp.css') !!}">
    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,900,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Anonymous+Pro|Raleway:400,600,700' rel='stylesheet' type='text/css'>
     <!-- Latest compiled and minified JavaScript -->
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    <script type='text/javascript'>
    $(function() { $(window).scroll(function() { if($(this).scrollTop()>100) { $('#BounceToTop').slideDown(200); } else { $('#BounceToTop').slideUp(300); } });
    $('#BounceToTop').click(function() { $('body,html').animate({scrollTop:0},800) .animate({scrollTop:25},200) .animate({scrollTop:0},150) .animate({scrollTop:10},100) .animate({scrollTop:0},50); }); });
    </script>
      <!-- Histats.com  START  (aync)-->
    <script type="text/javascript">var _Hasync= _Hasync|| [];
    _Hasync.push(['Histats.start', '1,3458701,4,0,0,0,00010000']);
    _Hasync.push(['Histats.fasi', '1']);
    _Hasync.push(['Histats.track_hits', '']);
    (function() {
    var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
    hs.src = ('//s10.histats.com/js15_as.js');
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
    })();</script>
    <noscript><a href="http://www.histats.com" target="_blank"><img  src="http://sstatic1.histats.com/0.gif?3458701&101" alt="unique visitors counter" border="0"></a></noscript>
    <!-- Histats.com  END  -->
    <script>
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

     ga('create', 'UA-72505768-1', 'auto');
     ga('send', 'pageview');

   </script>
  </head>
  <body>
  <header>

    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
     <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#burger-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href='{!!url ('/')!!}'>
                <img src="img/logofull_inversetransp-01.png">
              </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="navbar-collapse collapse" id="burger-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">About Us <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{!!url ('/#WhatIsEncirclo')!!}">What is Encirclo</a></li>
                <li><a href="{!!url ('/#HowItWorks')!!}">How It Works</a></li>
                <li><a href="{!!url ('vip')!!}">VIP</a></li>
                <li><a href='ourvalues'>Our Values</a></li>
              </ul>
              <li><a href="{!!url ('/#JoinUs')!!}" id='contact'>Join Us</a></li>
              <li><a href='contact' id='contact'>Contact Us</a></li>
          </ul>
        </div><!-- /.navbar-collapse
        </div><!-- /.container -->
        </nav>
      </nav>
    </header>
    <div class="konten kontenprivacy">
    <div class="judulprivacy">
      <a href="{!!url ('/')!!}" class="btn back">Back</a>
      <h1>Privacy Policy</h1>
    </div>
    <div class="isiprivacy">
      <section class="pengantar">
        <p class="edited">Last Edited May 15, 2016</p>
        <p class="awal">Encirclo.com is a business networking platform owned and operated by PT. Encirclo Nusa Integra (“Encirclo”,”us”,”we”,”our”). We provide our services that support business activities and networking for any business entity in the world, including startups, Small to Medium Enterprises, even big enterprises.  Our main priority is to improve company business activities and expand company network.
        </p>
        <p class="awal">Privacy policy is our commitment to honor and protect important information from www.encirclo.com (herinafter also referred to “our website”) subscribers. We appreciate subscribers confidentiality in terms of data and informations, either as personal or as company. All data and information that we collect from users are defined in this policy. Data and information will be processed at the time of registration, access, and using Encirclo services.
        </p>
      </section>
      <section class="isi">
        <ul class="poinbesar">
          <li class="besar"> A. Data and Information</li>
          <section class="isiA">
            <ul class="poinA">
              <li>We realize your privacy is important and ensure that we do not collect more data or information from you than is necessary. What we collect just to support our site in order to provide you with our service and protect your account.
              </li>
              <li>Encirclo collect information when user register on our site, use Encirclo services, visit our or certain partner page, and when you contact us. Any information from our partner can be gathered with our’s.
              </li>
              <li>Information including, but not limited to, Subscriber name, address , phone  number, email address, company legal and user preferences (‘registration information’) may be collected at the time of subscription.
              </li>
              <li>Information or data including, but not limited to, full description of business or project opportunity or any offering related to business by user through our sites, report of business execution by user, delivery information and any dispute records (“Activity information”) may be collected when activities are carried on or facilitated through our sites
              </li>
              <li>We record any note about correspondence and contents of communication between subscriber and encirclo when subscriber/encirclo make contact. We record any user browsing activities on our platform including, but not limited to, IP addresses, cookies information, browsing patterns, software and hardware attributes, page viewed (group as “Browsing Information”).
              </li>
              <li>Encirclo may collect information about users and prospective users during seminars, industry events, conference, and any other meeting. the information we collect at these encounter may include, but is not limited to, user name, address, phone number, fax number, and email address (“Event information”).
              </li>
              <li>It is required for subscribers to provide certain categories of business data and or personal data (collect when needed). In the event which specified data needed and users not provide any or adequate business data or personal data, we may not complete registration or provide such subscribers with our products or services.
              </li>
            </ul>
          </section>
          <li class="besar"> B. What Encirclo shares:</li>
          <section class="isiB">
            <ul class="poinB">
              <li>Business/Company Short Profile</li>
              <li>Projects</li>
              <li>Events</li>
              <li>Opportunities</li>
              <li>Other Business Information</li>
            </ul>
          </section>
          <li class="besar"> C. Minors</li>
          <section class="isiC">
            <ul class="poinC">
              <li>Encirclo.com and a variety of content in it is not targeted to minors (under the age of 18 years) and we do not intend to showcase all the products and services to minors. Still, we cannot distinguish the age of every user who access the site. If there is a minor who provide or provide personal information or business information without the consent of parents or authorized guardian, parents or guardian should contact us through info@encirclo.com to remove related information.
              </li>
            </ul>
          </section>
          <li class="besar"> D. Security Measures</li>
          <section class="isiD">
            <ul class="poinD">
              <li>We always cover the confidentiality of information you have provided, but we cannot guarantee the security of information you provide. We believe that you have already know the risk of every related information.
              </li>
            </ul>
          </section>
          <li class="besar"> E. Changes of Privacy Policy</li>
          <section class="isiE">
            <ul class="poinE">
              <li>By using our website or services that we provide, you agree to our activity that collect, use, distribute, and process the data you have entered by this privacy policy. This privacy policy will always be updated from time to time (current privacy policy will be directly used when it is released) and any changes will be shown on our site. You agree that any information related to you (as specified in this privacy policy or not collected prior to or after the privacy policy became effective) will be governed on the latest privacy policy.
              </li>
              <li>By agreeing Website Terms, in the event of inconsistency between the website terms and privacy policy, the privacy policy will be the first priority.
              </li>
            </ul>
          </section>
          <li class="besar"> F. Feedback</li>
          <section class="isiF">
            <ul class="poinF">
              <li>We are happy to welcome and accept any kind of subscribers input related to the "privacy policy" and all kinds of comments on the service we provide. Please send us feedback, suggestions and questions through our email info@encirclo.com and/or letters to our office at Jl. Cipinang Indah Raya no.1, Kalimalang, East Jakarta (Graha Komando Building).
              </li>
            </ul>
          </section>
        </ul>
      </section>
    </div>
    <style type='text/css' scoped='scoped'>
      #BounceToTop{position:fixed; bottom:50px; right:50px; cursor:pointer;display:none}
    </style>
      <div id='BounceToTop'><img alt='Back to top' src='img/arrows-3.png'/>
      </div>
  </div>

  	<!---------------------- THIS IS FOOTER  ------------------------------------>
  	<footer>
  		<div class="row ">
  			<div class="col-sm-6 kolom1" style="margin-top:15px">
  				<h1 class="footerh1"> E N C I R C L O </h1>
  				<h2>An Indonesian small company with huge dreams to change the way people do business all over the world. We won't stop until all pieces unite and collaborate in one place for goodness.</h2>
  			</div>
  			<!--------------------DIV DIATAS ADALAH Footer kolom kiri -------------------->
  			<div class="col-sm-6 kolom2">
  				<div class="rowatas">
  					<h5 style="color:white; letter-spacing:2px;"> Discover More</h5>
  					<ul class="">
  						<!-- <li class='footerbaris'>Discover More</li> -->
  						<li class='gambar'>
  							<a id='fb' href='https://www.facebook.com/encirclo' target='_blank'>
  								<img src="img/buttonfb6.png" alt='facebook'>
  							</a>
  						</li>
  						<li class='gambar'>
  							<a id='tw' href='https://www.twitter.com/encirclomedia' target='_blank'>
  								<img src="img/buttontw6.png" alt="twitter">
  							</a>
  						</li>
  						<li class='gambar'>
  							<a id='in' href='https://www.linkedin.com/company/pt--encirclo-nusa-integra?trk=top_nav_home' target='_blank'>
  								<img src="img/buttonin6.png" alt="linkedin">
  							</a>
  						</li>
  					</ul>
  				</div>
  				<div class="rowbawah">
  					<ul>
  						<li><a href="{!!url('terms-condition') !!}">Terms and Conditions</a></li>
  						<li><a href="{!!url('privacy') !!}">Privacy Policy</a></li>
  						<li><a href="{!!url('help') !!}">Help Center</a></li>
  					</ul>
  					<span> &copy; PT. Encirclo Nusa Integra. All Rights Reserved</span>
  				</div>
  			</div>
  		</div>
  	<!-----------------------DIV DIATAS ADALAH Footer kolom kanan bawah---------------------->
  	</footer>

</body>

  <div id="loader-wrapper">
  	<div id="loader"></div>

  	<div class="loader-section section-left"></div>
	  <div class="loader-section section-right"></div>

  </div>

  <!-- <script src="{!! url('script/script.js') !!}"></script> -->
  <script src="{!! url('js/jqBootstrapValidation.js') !!}"></script>


  <script type="text/javascript">

    $(document).ready(function() {
      // alert('ready');
      setTimeout(function(){
        $('body').addClass('loaded');
        // $('#loader-wrapper').css('display','none');

      }, 1500);

    });

  </script>

</html>
