<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VIP</title>
    <link rel="icon" href="{!! url('img/logo.png') !!}">

    <!-- BOOTSTRAP CDN -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- link jquery -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <!-- STYLE -->
    <link rel="stylesheet" href="{!! url('css/landingpage.css') !!}">
    <link rel="stylesheet" href="{!! url('css/medquery-lp.css') !!}">

    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Anonymous+Pro|Raleway:400,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,900,700' rel='stylesheet' type='text/css'>
     <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

      <script type='text/javascript'>
        $(function() { $(window).scroll(function() { if($(this).scrollTop()>100) { $('#BounceToTop').slideDown(200); } else { $('#BounceToTop').slideUp(300); } });
        $('#BounceToTop').click(function() { $('body,html').animate({scrollTop:0},800) .animate({scrollTop:25},200) .animate({scrollTop:0},150) .animate({scrollTop:10},100) .animate({scrollTop:0},50); }); });
      </script>

        <!-- Histats.com  START  (aync)-->
      <script type="text/javascript">var _Hasync= _Hasync|| [];
      _Hasync.push(['Histats.start', '1,3458701,4,0,0,0,00010000']);
      _Hasync.push(['Histats.fasi', '1']);
      _Hasync.push(['Histats.track_hits', '']);
      (function() {
      var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
      hs.src = ('//s10.histats.com/js15_as.js');
      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
      })();</script>
      <noscript><a href="http://www.histats.com" target="_blank"><img  src="http://sstatic1.histats.com/0.gif?3458701&101" alt="unique visitors counter" border="0"></a></noscript>
      <!-- Histats.com  END  -->
    <script>
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

     ga('create', 'UA-72505768-1', 'auto');
     ga('send', 'pageview');

   </script>
  </head>

  <body>
    <header>
      <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container-fluid">
       <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#burger-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href='{!!url ('/')!!}'>
                  <img src="img/logofull_inversetransp-01.png">
                </a>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="navbar-collapse collapse" id="burger-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">About Us <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{!!url ('/#WhatIsEncirclo')!!}">What is Encirclo</a></li>
                  <li><a href="{!!url ('/#HowItWorks')!!}">How It Works</a></li>
                  <li><a href="{!!url ('vip')!!}">VIP</a></li>
                  <li><a href='ourvalues'>Our Values</a></li>
                </ul>
                <li><a href="{!!url ('/#JoinUs')!!}" id='contact'>Join Us</a></li>
                <li><a href='contact' id='contact'>Contact Us</a></li>
            </ul>
          </div><!-- /.navbar-collapse
          </div><!-- /.container -->
          </nav>
      </div>
    </header>
    <div class="kontenvip">
      <!-- <a href="{!!url ('/')!!}" class="btn back" style="margin-top:35px;">Back</a> -->
      <div class="jasvip">
      </div>
      <div class="isivip">
        <img class="first-img" src="img/vip/vip1-01.png">
        <div class="deskripsi">
          <p>VIP, or Very Important Partner, is defined as the first business (might be represented as company or enterprise) which approved to be our special Partner. Declared as the special one because it is only limited to a certain number and period to get our exclusive services.
          </p>
        </div>
        <img src="img/vip/vip_hitam-01.png">
        <div class="standard">
          <p>To become our VIP, the business must be registered on our subscribe form. Chosen VIP will be approached via personal contact (phone, or email).
          </p>
          <p> Standards for VIPs are, but not limited to:
            <ul class="standard-list">
              <li> Formed as a business team or entity, including Startup, Small to Medium Enterprise, or Big Enterprise,</li>
              <li> Have no legal issues,</li>
              <li> Active Company,</li>
              <li> Have a clear purpose of business and source of revenue,</li>
              <li> Can be identified via website or personal contact,</li>
            </ul>
          </p>
          <p>Note: not all subscribers directly approved as our VIP. Yet we will properly select them in accordance with our VIP standards.
          </p>
        </div>
        <img src="img/vip/vip_hitam-02.png">
        <div class="benefit">
          <p>We aim to build awareness to your business existence. Moreover, we gladly to enable business collaboration between our VIPs. Before we establish our full featured platform, we would like to provide VIP promotion on various Encirclo marketing campaign media.
          </p>
          <p> Our marketing campaign will be spread on various media, either online (e.g. Facebook, LinkedIn, Twitter, Instagram, email newsletter), or offline (word-of-mouth communication). Contents of our marketing campaign are:
            <ul class="benefit-list">
              <li> Business/Company Short Profile,</li>
              <li> Projects,</li>
              <li> Events,</li>
              <li> Opportunities,</li>
              <li> Other Business Information,</li>
            </ul>
          </p>
          <p>In addition, we will let VIP a premium access to our full featured platform. VIPs will be the first who experience the power of business networking.
          </p>
          <p>So, subscribe your business, keep connected with Encirclo, and get ready to be the first 50 VIP!
          </p>
        </div>
        <a href="{!!url ('/#JoinUs')!!}" class="btn2" style="margin-bottom:70px; margin-top:50px;">Subscribe My Company</a>
      </div>
    </div>
  	<!---------------------- THIS IS FOOTER  ------------------------------------>
  	<footer>
  		<div class="row ">
  			<div class="col-sm-6 kolom1" style="margin-top:15px">
  				<h1 class="footerh1"> E N C I R C L O </h1>
  				<h2>An Indonesian small company with huge dreams to change the way people do business all over the world. We won't stop until all pieces unite and collaborate in one place for goodness.</h2>
  			</div>
  			<!--------------------DIV DIATAS ADALAH Footer kolom kiri -------------------->
  			<div class="col-sm-6 kolom2">
  				<div class="rowatas">
  					<h5 style="color:white; letter-spacing:2px;"> Discover More</h5>
  					<ul class="">
  						<!-- <li class='footerbaris'>Discover More</li> -->
  						<li class='gambar'>
  							<a id='fb' href='https://www.facebook.com/encirclo' target='_blank'>
  								<img src="img/buttonfb6.png" alt='facebook'>
  							</a>
  						</li>
  						<li class='gambar'>
  							<a id='tw' href='https://www.twitter.com/encirclomedia' target='_blank'>
  								<img src="img/buttontw6.png" alt="twitter">
  							</a>
  						</li>
  						<li class='gambar'>
  							<a id='in' href='https://www.linkedin.com/company/pt--encirclo-nusa-integra?trk=top_nav_home' target='_blank'>
  								<img src="img/buttonin6.png" alt="linkedin">
  							</a>
  						</li>
  					</ul>
  				</div>
  				<div class="rowbawah">
  					<ul>
  						<li><a href="{!!url('terms-condition') !!}">Terms and Conditions</a></li>
  						<li><a href="{!!url('privacy') !!}">Privacy Policy</a></li>
  						<li><a href="{!!url('help') !!}">Help Center</a></li>
  					</ul>
  					<span> &copy; PT. Encirclo Nusa Integra. All Rights Reserved</span>
  				</div>
  			</div>
  		</div>
  	<!-----------------------DIV DIATAS ADALAH Footer kolom kanan bawah---------------------->
  	</footer>

  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){

  if (screen.width<992) {
    var a = window.height;
    $('.judulabout').css('height',a);
    $('.kontenabout .isiabout').css('top',a);
  }
});
</script>
</body>
