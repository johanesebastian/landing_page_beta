<html>
  <head>
    <meta charset="utf-8">
    <title>Terms and Condition</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- BOOTSTRAP CDN -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- link jquery -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <!-- STYLE -->
    <link rel="stylesheet" href="{!! url('css/landingpage.css') !!}">
    <link rel="stylesheet" href="{!! url('css/medquery-lp.css') !!}">

    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,900,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Anonymous+Pro|Raleway:400,600,700' rel='stylesheet' type='text/css'>
     <!-- Latest compiled and minified JavaScript -->
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

      <script type='text/javascript'>
      $(function() { $(window).scroll(function() { if($(this).scrollTop()>100) { $('#BounceToTop').slideDown(200); } else { $('#BounceToTop').slideUp(300); } });
      $('#BounceToTop').click(function() { $('body,html').animate({scrollTop:0},800) .animate({scrollTop:25},200) .animate({scrollTop:0},150) .animate({scrollTop:10},100) .animate({scrollTop:0},50); }); });
      </script>
        <!-- Histats.com  START  (aync)-->
      <script type="text/javascript">var _Hasync= _Hasync|| [];
      _Hasync.push(['Histats.start', '1,3458701,4,0,0,0,00010000']);
      _Hasync.push(['Histats.fasi', '1']);
      _Hasync.push(['Histats.track_hits', '']);
      (function() {
      var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
      hs.src = ('//s10.histats.com/js15_as.js');
      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
      })();</script>
      <noscript><a href="http://www.histats.com" target="_blank"><img  src="http://sstatic1.histats.com/0.gif?3458701&101" alt="unique visitors counter" border="0"></a></noscript>
      <!-- Histats.com  END  -->
    <script>
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

     ga('create', 'UA-72505768-1', 'auto');
     ga('send', 'pageview');

   </script>
  </head>
  <body>
  <header>

    <nav class="navbar navbar-default navbar-fixed-top">
  			<div class="container-fluid">
  	 <!-- Brand and toggle get grouped for better mobile display -->
  			<div class="navbar-header">
  						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#burger-collapse">
  							<span class="icon-bar"></span>
  							<span class="icon-bar"></span>
  							<span class="icon-bar"></span>
  						</button>
  						<a class="navbar-brand" href='{!!url ('/')!!}'>
  							<img src="img/logofull_inversetransp-01.png">
  						</a>
  			</div>
  			<!-- Collect the nav links, forms, and other content for toggling -->
  			<div class="navbar-collapse collapse" id="burger-collapse">
  				<ul class="nav navbar-nav navbar-right">
  					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">About Us <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{!!url ('/#WhatIsEncirclo')!!}">What is Encirclo</a></li>
                <li><a href="{!!url ('/#HowItWorks')!!}">How It Works</a></li>
                <li><a href="{!!url ('vip')!!}">VIP</a></li>
  							<li><a href='ourvalues'>Our Values</a></li>
              </ul>
  						<li><a href="{!!url ('/#JoinUs')!!}" id='contact'>Join Us</a></li>
  						<li><a href='contact' id='contact'>Contact Us</a></li>
  				</ul>
  			</div><!-- /.navbar-collapse
  			</div><!-- /.container -->
  			</nav>
      </nav>
    </header>
    <div class="konten kontenterms">
    <div class="judulterms">
      <a href="{!!url ('/')!!}" class="btn back">Back</a>
      <h1>Terms and Conditions</h1>
    </div>
    <div class="isiterms">
      <section class="pengantar">
        <p class="edited">Last Edited May 15, 2016</p>
        <p class="awal">This Subscriber “Terms and Conditions” determines the www.encirclo.com website. If subscriber (herinafter also referred to “you”) submit a subscription to our website, you are deemed to have read, understand, and approve all parts of this Terms and Conditions. Please read carefully before you finalize the submission.
        </p>
      </section>
      <section class="isi">
        <ul class="poinbesar">
          <li class="besar"> A. General Information</li>
          <section class="isiA">
            <ul class="poinA">
              <li>PT. Encirclo Nusa Integra (“Encirclo”,”us”,”we”,”our”) is an incorporated company that runs a website service www.encirclo.com (herinafter also referred to “our website”) which is a business networking platform for companies.
              </li>
              <li>This Terms and Conditions is an agreement between subscriber and Encirclo which contains a set of rules governing the rights, obligations, responsibilities and Encirclo subscribers, as well as the procedures for our website service system until we launch our full feature website.
              </li>
            </ul>
          </section>
          <li class="besar"> B. Subscriber</li>
          <section class="isiB">
            <ul class="poinB">
              <li>Subscriber must fill out complete and truthful personal data in the subscription form.</li>
              <li>You officially become a subscriber of our website by completing the subscription form without any error value and verify your subscription in your email mentioned in the subscription form.
              </li>
              <li>Subscriber will automatically get email campaigns containing various informations such as our website development, list of companies that have joined Encirclo, website launch date, and other business information. Yet, subscribers are also allowed to unsubscribe our email campaign anytime.
              </li>
              <li>Subscriber not allowed to send spam email refer to any part of our website.
              </li>
            </ul>
          </section>
          <li class="besar"> C. VIP</li>
          <section class="isiC">
            <ul class="poinC">
              <li>VIP (Very Important Partner) is defined as the first company/business which approved as our Partner to get special services, including promotion on various Encirclo marketing campaign media.
              </li>
              <li>VIP must be registered on subscribers list or previously had been approved by Encirclo as a Partner.
              </li>
              <li>Not all subscribers directly approved as VIP. Selection will be adjusted with VIP standards.
              </li>
              <li>Standards for VIP are, but not limited to:</li>
                <ul class="poinC2">
                  <li>a. Formed as a business team or entity, including Startup, Small to Medium Enterprise, or Big Enterprise,
                  </li>
                  <li>b. Have no legal issues,
                  </li>
                  <li>c. Active Company,
                  </li>
                  <li>d. Have a clear purpose of business and source of revenue,
                  </li>
                  <li>e. Can be identified via website or personal contact,
                  </li>
                </ul>
              <li>Chosen VIP will be approached via personal contact (phone, or email).
              </li>
              <li>Further collaboration will be discussed and written in another agreement(s).
              </li>
              <li>Encirclo will not take resposibility for any issues related to VIP.
              </li>
            </ul>
          </section>
          <li class="besar"> D. Marketing Campaign</li>
          <section class="isiD">
            <ul class="poinD">
              <li>Encirclo Marketing Campaign will be done on various media, either online (e.g. Facebook, LinkedIn, Twitter, Instagram, email, etc), or offline.
              </li>
              <li>Contents of Marketing Campaign are, but not limited to:</li>
                <ul class="poinD2">
                  <li>a. Business/Company Short Profile</li>
                  <li>b. Projects
                  </li>
                  <li>c. Events
                  </li>
                  <li>d. Opportunities</li>
                  <li>e. Other Business Information
                  </li>
                </ul>
              <li>Marketing campaign services for VIP are free of charge, unless for a particular demand outside the agreement.
              </li>
              <li>Time and frequency of Marketing Campaign will be discussed on further agreement.
              </li>
            </ul>
          </section>
          <li class="besar"> E. Miscellaneous
          </li>
          <section class="isiE">
            <ul class="poinE">
              <li>Terms and conditions may be changed and / or updated without prior notice. We recommend that you read carefully and periodically check this page Terms & conditions for any changes. By continuing to access and use services Encirclo, then subscribers are considered to approve the changes in the Terms and Conditions.
              </li>
            </ul>
          </section>
        </ul>
      </section>
    </div>

    <style type='text/css' scoped='scoped'>
      #BounceToTop{position:fixed; bottom:50px; right:50px; cursor:pointer;display:none}
    </style>
      <div id='BounceToTop'><img alt='Back to top' src='img/arrows-3.png'/>
      </div>

  </div>


  	<!---------------------- THIS IS FOOTER  ------------------------------------>
  	<footer>
  		<div class="row ">
  			<div class="col-sm-6 kolom1" style="margin-top:15px">
  				<h1 class="footerh1"> E N C I R C L O </h1>
  				<h2>An Indonesian small company with huge dreams to change the way people do business all over the world. We won't stop until all pieces unite and collaborate in one place for goodness.</h2>
  			</div>
  			<!--------------------DIV DIATAS ADALAH Footer kolom kiri -------------------->
  			<div class="col-sm-6 kolom2">
  				<div class="rowatas">
  					<h5 style="color:white; letter-spacing:2px;"> Discover More</h5>
  					<ul class="">
  						<!-- <li class='footerbaris'>Discover More</li> -->
  						<li class='gambar'>
  							<a id='fb' href='https://www.facebook.com/encirclo' target='_blank'>
  								<img src="img/buttonfb6.png" alt='facebook'>
  							</a>
  						</li>
  						<li class='gambar'>
  							<a id='tw' href='https://www.twitter.com/encirclomedia' target='_blank'>
  								<img src="img/buttontw6.png" alt="twitter">
  							</a>
  						</li>
  						<li class='gambar'>
  							<a id='in' href='https://www.linkedin.com/company/pt--encirclo-nusa-integra?trk=top_nav_home' target='_blank'>
  								<img src="img/buttonin6.png" alt="linkedin">
  							</a>
  						</li>
  					</ul>
  				</div>
  				<div class="rowbawah">
  					<ul>
  						<li><a href="{!!url('terms-condition') !!}">Terms and Conditions</a></li>
  						<li><a href="{!!url('privacy') !!}">Privacy Policy</a></li>
  						<li><a href="{!!url('help') !!}">Help Center</a></li>
  					</ul>
  					<span> &copy; PT. Encirclo Nusa Integra. All Rights Reserved</span>
  				</div>
  			</div>
  		</div>
  	<!-----------------------DIV DIATAS ADALAH Footer kolom kanan bawah---------------------->
  	</footer>

</body>

</html>
