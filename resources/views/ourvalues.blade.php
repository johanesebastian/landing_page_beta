<html>
  <head>
    <meta charset="utf-8">
    <title>Our Values</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <!-- BOOTSTRAP CDN -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- link jquery -->
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <!-- STYLE -->
    <link rel="stylesheet" href="{!! url('css/landingpage.css') !!}">
    <link rel="stylesheet" href="{!! url('css/medquery-lp.css') !!}">

    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Anonymous+Pro|Raleway:400,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,900,700' rel='stylesheet' type='text/css'>
     <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

      <script type='text/javascript'>
        $(function() { $(window).scroll(function() { if($(this).scrollTop()>100) { $('#BounceToTop').slideDown(200); } else { $('#BounceToTop').slideUp(300); } });
        $('#BounceToTop').click(function() { $('body,html').animate({scrollTop:0},800) .animate({scrollTop:25},200) .animate({scrollTop:0},150) .animate({scrollTop:10},100) .animate({scrollTop:0},50); }); });
      </script>

        <!-- Histats.com  START  (aync)-->
      <script type="text/javascript">var _Hasync= _Hasync|| [];
      _Hasync.push(['Histats.start', '1,3458701,4,0,0,0,00010000']);
      _Hasync.push(['Histats.fasi', '1']);
      _Hasync.push(['Histats.track_hits', '']);
      (function() {
      var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
      hs.src = ('//s10.histats.com/js15_as.js');
      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
      })();</script>
      <noscript><a href="http://www.histats.com" target="_blank"><img  src="http://sstatic1.histats.com/0.gif?3458701&101" alt="unique visitors counter" border="0"></a></noscript>
      <!-- Histats.com  END  -->
    <script>
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

     ga('create', 'UA-72505768-1', 'auto');
     ga('send', 'pageview');

   </script>
  </head>

  <body>
    <header>
      <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container-fluid">
       <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#burger-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href='{!!url ('/')!!}'>
                  <img src="img/logofull_inversetransp-01.png">
                </a>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="navbar-collapse collapse" id="burger-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">About Us <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{!!url ('/#WhatIsEncirclo')!!}">What is Encirclo</a></li>
                  <li><a href="{!!url ('/#HowItWorks')!!}">How It Works</a></li>
                  <li><a href="{!!url ('vip')!!}">VIP</a></li>
                  <li><a href='ourvalues'>Our Values</a></li>
                </ul>
                <li><a href="{!!url ('/#JoinUs')!!}" id='contact'>Join Us</a></li>
                <li><a href='contact' id='contact'>Contact Us</a></li>
            </ul>
          </div><!-- /.navbar-collapse
          </div><!-- /.container -->
          </nav>
      </div>
    </header>

  <div class="kontenabout">
    <!-- <div class="judulabout-alas"> -->
      <div class="judulabout">
        <a href="{!!url ('/')!!}" class="btn back" style="margin-top:35px;">Back</a>
        <h1>Welcome to The New Age of Business Networking</h1>
        <section class="pengantar">
          <p class="awal"> As a platform, we present to facilitate, connect, and collaborate your business with others in order to create global business environment. We want to create a modern way of business and we want you to participate with us. Would you mind if another business establish bigger network and connection to yours? Would you like to create an impactful change for the world?
          </p>
        </section>
      <!-- </div> -->
      <!-- <div class="boxatas-isiabout">
        <p> Atmosphere Behavior Culture </P>
      </div> -->
    </div>
    <div class="isiabout">
      <div class="kiri">
        <div>
          <h1>Our Philosophy</h1>
          <p>We simply consider every business entity is just like piece of puzzle. Is it possible for a piece to stand alone? Even the biggest piece still need another to create something valuable. Just like your business, we consider ourself as piece of puzzle and every piece need to be united in order to generate significant result and impact to society.
          </p>
        </div>
        <img src="img/visi/Philosophy.png" alt="Philosophy">
      </div>
      <div class="kanan">
        <img src="img/visi/Vision.png" alt="Vision">
        <div>
          <h1>Vision</h1>
          <p><i>Creating a global business network with networking efficiently concept.</i>
          </p>
          <p>Is it possible to unite every business in one place to build a sustainable global business environment? We believe in you.</p>
        </div>
      </div>
      <div class="kiri">
        <div>
          <h1>Mission</h1>
          <p><i>Expanding network between businesses to fulfill its needs through ideal and beneficial collaboration.</i></p>
          <p>We want to connect your business with others and strengthen your ability to expand greater network. We won’t stop until no piece left behind.
          </p>
        </div>
        <img src="img/visi/Mission.png" alt="Mission">
      </div>
      <div class="kanan">
        <img src="img/visi/DNA.png" alt="DNA">
        <div class="dna"><h1 style="text-align:center">"PIECES" of Our DNA</h1>
          <p>Every person in Encirclo is unique with high diversity and that's what make us complete. Although all of us born to be unique, we all have PIECES in common as our DNA.</p>
          <ul>
            <li><strong>P</strong> rofessional</li>
            <li><strong>I</strong> ntegrity</li>
            <li><strong>E</strong> fficient</li>
            <li><strong>C</strong> reative</li>
            <li><strong>E</strong> njoy</li>
            <li><strong>S</strong> hare</li>
          </ul>
        </div>
      </div>
      <div class="kiri">
        <div><h1>Company History</h1>
          <p>We are proudly made in Indonesia. Founded by extraordinary persons in a small room and named after Orion at first. We change our name to Enterprise Circle Online (Encirclo). We believe no matter how big the business is, it could be as big as enterprises when they have found the right access and partner. Build and expand a network should be easy with us.
          </p>
        </div>
        <img src="img/visi/CompInfo.png" alt="History">
      </div>
    <!-- back to top -->
    <style type='text/css' scoped='scoped'>
      #BounceToTop{position:fixed; bottom:50px; right:50px; cursor:pointer;display:none; z-index: 3;}
    </style>
    <div id='BounceToTop'><img alt='Back to top' src='img/arrows-3.png'/></div>

  	<!---------------------- THIS IS FOOTER  ------------------------------------>
  	<footer>
  		<div class="row ">
  			<div class="col-sm-6 kolom1" style="margin-top:15px">
  				<h1 class="footerh1"> E N C I R C L O </h1>
  				<h2>An Indonesian small company with huge dreams to change the way people do business all over the world. We won't stop until all pieces unite and collaborate in one place for goodness.</h2>
  			</div>
  			<!--------------------DIV DIATAS ADALAH Footer kolom kiri -------------------->
  			<div class="col-sm-6 kolom2">
  				<div class="rowatas">
  					<h5 style="color:white; letter-spacing:2px;"> Discover More</h5>
  					<ul class="">
  						<!-- <li class='footerbaris'>Discover More</li> -->
  						<li class='gambar'>
  							<a id='fb' href='https://www.facebook.com/encirclo' target='_blank'>
  								<img src="img/buttonfb6.png" alt='facebook'>
  							</a>
  						</li>
  						<li class='gambar'>
  							<a id='tw' href='https://www.twitter.com/encirclomedia' target='_blank'>
  								<img src="img/buttontw6.png" alt="twitter">
  							</a>
  						</li>
  						<li class='gambar'>
  							<a id='in' href='https://www.linkedin.com/company/pt--encirclo-nusa-integra?trk=top_nav_home' target='_blank'>
  								<img src="img/buttonin6.png" alt="linkedin">
  							</a>
  						</li>
  					</ul>
  				</div>
  				<div class="rowbawah">
  					<ul>
  						<li><a href="{!!url('terms-condition') !!}">Terms and Conditions</a></li>
  						<li><a href="{!!url('privacy') !!}">Privacy Policy</a></li>
  						<li><a href="{!!url('help') !!}">Help Center</a></li>
  					</ul>
  					<span> &copy; PT. Encirclo Nusa Integra. All Rights Reserved</span>
  				</div>
  			</div>
  		</div>
  	<!-----------------------DIV DIATAS ADALAH Footer kolom kanan bawah---------------------->
  	</footer>

  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){

  if (screen.width<992) {
    var a = window.height;
    $('.judulabout').css('height',a);
    $('.kontenabout .isiabout').css('top',a);
  }
});
</script>
</body>
