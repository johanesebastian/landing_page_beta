<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MailController extends Controller
{   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendNetworkMail(Request $request)
    {
        $user = $request->all;
        Mail::send('emails.join', ['user' => $user], function ($m) {
            $m->to('network@encirclo.com', 'Encirclo')->subject('Seseorang telah mendaftar Encirclo');
        });

        return url('/');
    }

    public function sendContactMail(Request $request)
    {
        $user = $request->all;
        Mail::send('emails.contact', ['user' => $user], function ($m) {
            $m->to('network@encirclo.com', 'Encirclo')->subject('Seseorang telah mengontak Encirclo');
        });

        return url('/');
    }

}
