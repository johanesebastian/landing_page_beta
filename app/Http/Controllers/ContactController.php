<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Contact;

class ContactController extends Controller
{
    public function submitContact(Request $request){
      if($request->ajax()) {
        $contact = new Contact;
        $contact->fill($request->all());
        $contact->save();
      }
    }
}
