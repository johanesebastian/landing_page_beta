<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('landingpage');
});


Route::get('contact', function () {
	return view('contact');
});

Route::get('privacy', function () {
	return view('privacy');
});

Route::get('terms-condition', function () {
	return view('termscondition');
});

Route::get('ourvalues', function () {
	return view('ourvalues');
});

Route::get('vip', function () {
	return view('vip');
});

Route::get('help', function () {
	return view('help');
});

Route::get('/', function () {
	return view('landingpage');
});



Route::post('contact', ['uses' => 'ContactController@submitContact']);



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
